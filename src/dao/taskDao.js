const Task = require("../models/taskModel");
const Board = require("../models/boardModel");


exports.createTaskDao = async (body) => {
    try {

        return await Task.create(body);

    } catch (error) {
        throw new Error(error);
    }
};

exports.listTaskDao = async (query) => {
    try {
        return await Board.aggregate([{
            $lookup:
            {
                from: "tasks",
                localField: "_id",
                foreignField: "boardId",
                as: "groupData"
            }
        }])

    } catch (error) {
        throw new Error(error);
    }
}



exports.changeTaskStatusDao = async (body) => {
    try {

        let status = await Task.findOne({ _id: body.taskId });

        if (status.currentStatus != body.currentStatus) {
            throw new Error("current status does not match");
        }
        await Task.updateOne({ _id: body.taskId }, { $set: { currentStatus: body.newStatus } });

        return await Task.findOne({ _id: body.taskId });

    } catch (error) {
        throw new Error(error);
    }
};