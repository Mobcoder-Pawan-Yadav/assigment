const User = require("../models/userModel");
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');




exports.userSignUpDao = async (body) => {
    try {
        const salt = await bcrypt.genSalt(10);

        body.password = await bcrypt.hash(body.password, salt);

        return await User.create(body);

    } catch (error) {
        throw new Error(error);
    }
};

exports.userLoginUpDao = async (body) => {
    try {
        let userData = await User.findOne({ email: body.email }).lean();
        const hash = userData.password;

        const isValidPass = await comparePassword(body.password, hash);

        if (!userData || !isValidPass) {
            throw new Error("email or password is incorrect")
        }

        const token = jwt.sign(userData, process.env.SECRET, { expiresIn: "1h" });
        return [{ accessToken: token }, userData];

    } catch (error) {
        throw new Error(error);
    }
}


const comparePassword = async (password, hash) => {
    try {

        return await bcrypt.compare(password, hash);
    } catch (error) {
        console.log(error);
    }

    return false;
};
