const Board = require("../models/boardModel");


exports.createBoardDao = async (body) => {
    try {
   
        return await Board.create(body);

    } catch (error) {
        throw new Error(error);
    }
};

exports.getListDao=async(body)=>{
    try {
        console.log(body)
        return await Board.find({userId:body.userId}).lean();

    } catch (error) {
        throw new Error(error);
    }
}

