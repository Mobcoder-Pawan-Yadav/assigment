const AppError = require("../appUtils/errorHandelers");
const { createBoardDao,getListDao } = require('../dao/boardDao');


exports.createBoardService = async (userInfo) => {
    try {
        
        return await createBoardDao(userInfo);

    } catch (error) {
        throw new AppError(400, 'fail', error.message);
    }

}

exports.getListService=async(userInfo)=>{
    try {
        
        return await getListDao(userInfo);

    } catch (error) {
        throw new AppError(400, 'fail', error.message);
    }
}
