const AppError = require("../appUtils/errorHandelers");
const { userSignUpDao,userLoginUpDao } = require('../dao/userDao');


exports.userSignUpService = async (userInfo) => {
    try {
        
        return await userSignUpDao(userInfo);

    } catch (error) {
        throw new AppError(400, 'fail', error.message);
    }

}

exports.useLoginService=async(userInfo)=>{
    try {
        
        return await userLoginUpDao(userInfo);

    } catch (error) {
        throw new AppError(400, 'fail', error.message);
    }
}
