const AppError = require("../appUtils/errorHandelers");
const { createTaskDao,listTaskDao,changeTaskStatusDao } = require('../dao/taskDao');


exports.createTaskService = async (taskInfo) => {
    try {
        
        return await createTaskDao(taskInfo);

    } catch (error) {
        throw new AppError(400, 'fail', error.message);
    }

}

exports.listTaskService=async(params)=>{
    try {
        
        return await listTaskDao(params);

    } catch (error) {
        throw new AppError(400, 'fail', error.message);
    }
}

exports.changeTaskStatusService=async(userInfo)=>{
    try {

        return await changeTaskStatusDao(userInfo);

    } catch (error) {
        throw new AppError(400, 'fail', error.message);
    }
}

