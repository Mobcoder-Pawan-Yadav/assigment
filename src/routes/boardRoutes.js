const express = require('express');
const router = express.Router();
const {create,list}=require('../controllers/boardController')

const {authenticate}=require("../middlewares/authenticate");

//to prevent action if user does not have a valid accesstoken
router.use(authenticate);


router.post("/create",create);
router.get("/list",list);

module.exports = router;
