const express = require('express');
const router = express.Router();
const {userSignUp,userLogin}=require('../controllers/userController');


router.post("/signup",userSignUp);
router.post("/login",userLogin);

//just to check if api is running on heroku
router.post("/check",(req,res)=>{
    res.send({message:"Api running"})
});


module.exports = router;
