const express = require('express');
const router = express.Router();
const {createTask,listTask,changeTaskStatus}=require('../controllers/taskController');
const {authenticate}=require("../middlewares/authenticate");

//to prevent action if user does not have a valid accesstoken
router.use(authenticate);


router.post("/create",createTask);
router.get("/list",listTask);
router.post("/changeStatus",changeTaskStatus)


module.exports = router;
