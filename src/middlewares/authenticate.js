const User =require("../models/userModel");
const{promisify}=require('util');
const jwt=require('jsonwebtoken');


exports.authenticate=async(req,res,next)=>{
    try{
        if(!req.headers.authorization){
            throw new Error("accessToken is required for next action")
        }
        const token = req.headers.authorization.split(" ")[0];
        const decode = await promisify(jwt.verify)(token, process.env.SECRET);

        let isUserExists=await User.findOne({_id:decode._id});
        
        if(!isUserExists){
            throw new Error("Not a valid acessToken, please provide a valid token");
        }
        next();
    }
    catch(error){
        next(error)
    }
}