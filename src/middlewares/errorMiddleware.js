module.exports = (err, req, res, next) => {
    err.statusCode = err.statusCode || 500;
    err.status = 'fail';
    const regex = /[^:]+$/;
   const found = err.message?.match(regex)[0];

    res.status(err.statusCode).json({
        status: err.status,
        error: err,
        message: found,
        
    });

};