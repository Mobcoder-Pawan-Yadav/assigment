class AppError extends Error {
    constructor(statusCode, status, message,name) {
        super(message);
        this.statusCode = statusCode;
        this.status = status;
        name==="TypeError"? this.message = "Parameters Incomplete": 
        this.message =message
        
    }
}

module.exports = AppError;